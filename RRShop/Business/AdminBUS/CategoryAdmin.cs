﻿using Connection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.AdminBUS
{
    public class CategoryAdmin
    {
        
        public static IEnumerable<Category> LoadAllCategory()
        {
            var db = new ConnectionDB();
            string queryString = @"SELECT CategoryID, CategoryName, Image, Banner
                                    FROM Category
                                    WHERE IsRemoved = 0";
            var list = db.Query<Category>(queryString);

            return list;
        }

        public static int AddCategory(Category cat)
        {
            Object item;
            cat.RouteName = cat.CategoryName.ToUnsign();
            using (var db = new ConnectionDB())
            {
                item = db.Insert("Category", "CategoryID", cat);
            }
            int id = int.Parse(item.ToString());

            return id;
        }

        public static bool UpdateCategory(Category cat)
        {
            var db = new ConnectionDB();
            cat.RouteName = cat.CategoryName.ToUnsign();

            IEnumerable<string> str = new string[] { "CategoryName", "RouteName" };
            int row = db.Update(cat, cat.CategoryID, str);

            if (row == 0)
                return false;

            return true;
        }

        public static bool DeleteCategory(int CatID)
        {
            var db = new ConnectionDB();
            int row = db.Update<Category>("SET IsRemoved=1 Where CategoryID=@0", CatID);

            if (row == 0)
                return false;

            return true;
        }

        public static Category GetCategoryByCatID(int CatID)
        {
            var db = new ConnectionDB();
            string queryString = @"SELECT CategoryID, CategoryName, RouteName
                                    FROM Category
                                    WHERE CategoryID = @0 AND IsRemoved = 0";
            var cat = db.SingleOrDefault<Category>(queryString, CatID);

            return cat;
        }

        public static bool UpdateBannerCategory(Category cat)
        {
            var db = new ConnectionDB();
            cat.RouteName = cat.CategoryName.ToUnsign();

            IEnumerable<string> str = new string[] { "banner" };
            int row = db.Update(cat, cat.CategoryID, str);

            if (row == 0)
                return false;

            return true;
        }
    }
}
