﻿using Business.ViewModels;
using Connection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.AdminBUS
{
    public class RoleAdmin
    {
        public static IEnumerable<RoleViewModel> GetAllRole()
        {
            var db = new ConnectionDB();
            string queryString = @"SELECT Id, Name
                                    FROM Role";
            var list = db.Query<RoleViewModel>(queryString);

            return list;
        }
    }
}
