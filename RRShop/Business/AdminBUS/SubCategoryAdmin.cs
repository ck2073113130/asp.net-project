﻿using Business.ViewModels;
using Connection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.AdminBUS
{
    public class SubCategoryAdmin
    {
        public static IEnumerable<SubCategoryViewModels> GetSubCategoriesBySubCategory(int SubCategoryID)
        {
            IEnumerable<SubCategoryViewModels> result;

            using (var db = new ConnectionDB())
            {
                string query = @"Select SubCategoryID, SubCategoryName, RouteName
                                 From SubCategory
                                 Where CategoryID = (Select CategoryID from SubCategory sub where SubCategoryID = @0) AND
		                                IsRemoved = 0";

                result = db.Query<SubCategoryViewModels>(query, SubCategoryID);
            }

            return result;
        }

        public static IEnumerable<SubCategory> GetSubCategoryByCategory(int CategoryID)
        {

            IEnumerable<SubCategory> result;

            using (var db = new ConnectionDB())
            {
                string query = @"Select SubCategoryID, SubCategoryName
                                 From SubCategory
                                 Where CategoryID = @0 AND IsRemoved = 0";

                result = db.Query<SubCategory>(query, CategoryID);
            }

            return result;
        }

        public static IEnumerable<SubCategoryViewModels> LoadAllSubCategory()
        {
            var db = new ConnectionDB();
            string queryString = @"SELECT sub.SubCategoryID, sub.SubCategoryName, sub.RouteName, cat.CategoryID, cat.CategoryName
                                    FROM SubCategory sub, Category cat
                                    WHERE sub.CategoryID = cat.CategoryID AND sub.IsRemoved = 0";
            var list = db.Query<SubCategoryViewModels>(queryString);

            return list;
        }

        public static int AddSubCategory(SubCategory sub)
        {
            if(sub.CategoryID == null)
            {
                return 0;
            }

            Object item;
            sub.RouteName = sub.SubCategoryName.ToUnsign();
            using (var db = new ConnectionDB())
            {
                item = db.Insert("SubCategory", "SubCategoryID", sub);
            }
            int id = int.Parse(item.ToString());

            return id;
        }

        public static bool UpdateSubCategory(SubCategory sub)
        {
            var db = new ConnectionDB();
            sub.RouteName = sub.SubCategoryName.ToUnsign();

            IEnumerable<string> str = new string[] { "SubCategoryName", "RouteName" , "CategoryID"};
            int row = db.Update(sub, sub.SubCategoryID, str);

            if (row == 0)
                return false;

            return true;
        }

        public static bool DeleteSubCategory(int SubCategoryID)
        {
            var db = new ConnectionDB();
            int row = db.Update<SubCategory>("SET IsRemoved=1 Where SubCategoryID=@0", SubCategoryID);

            if (row == 0)
                return false;

            return true;
        }

    }
}
