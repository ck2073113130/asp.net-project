﻿using Business.ViewModels;
using Connection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.AdminBUS
{
    public class UserRoleAdmin
    {
        public static IEnumerable<UserRoleViewModel> GetRoleByUser(string UserID)
        {
            var db = new ConnectionDB();
            string queryString = @"SELECT UserId, Id, Name
                                    FROM UserRole right join Role on 
		                                    UserRole.RoleId = Role.Id And 
		                                    UserId = @0";
            var list = db.Query<UserRoleViewModel>(queryString, UserID);

            return list;
        }

        public static bool InsertUserRole(UserRoleViewModel UserRole)
        {
            var db = new ConnectionDB();
            string queryString = @"Insert into UserRole(UserId, RoleId)
                                    Values(@0, @1)";
            string[] param = { UserRole.UserId, UserRole.RoleId };
            int row = db.Execute(queryString, param);

            if (row == 0)
                return false;

            return true;
        }

        public static bool DeleteUserRole(UserRoleViewModel UserRole)
        {
            var db = new ConnectionDB();
            string queryString = @"Delete UserRole
                                    Where UserId = @0 And RoleId = @1";
            string[] param = { UserRole.UserId, UserRole.RoleId };
            int row = db.Execute(queryString, param);

            if (row == 0)
                return false;

            return true;
        }

        public static bool DeleteAllUserRoleByUserId(string UserId)
        {
            var db = new ConnectionDB();
            string queryString = @"Delete UserRole
                                    Where UserId = @0";
            
            int row = db.Execute(queryString, UserId);

            if (row == 0)
                return false;

            return true;
        }
    }
}
