﻿using Connection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Business
{
    public class ImageBUS
    {
        /// <summary>
        /// Gets Image List
        /// </summary>
        /// <param name="id">Product ID</param>
        /// <returns></returns>
        public static List<Image> GetImagesByProduct(int id)
        {
            List<Image> result;

            using (var db = new ConnectionDB())
            {
                result = db.Query<Image>(@"Select Link From Image Where ProductID = @0 Order By IsMainImage DESC", id).ToList();
            }

            return result;
        }

        /// <summary>
        /// Gets an Image
        /// </summary>
        /// <param name="id">Product ID</param>
        /// <returns></returns>
        public static List<Image> GetImageByProduct(int id)
        {
            List<Image> result;

            using (var db = new ConnectionDB())
            {
                result = db.Query<Image>(@"Select Link From Image Where ProductID = @0 AND IsMainImage = 1", id).ToList();
            }

            return result;
        }
    }
}
