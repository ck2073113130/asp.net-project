﻿using Business.ViewModels;
using Connection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Business
{
    public class InvoiceBUS
    {
        public static bool Insert(string userId, string address, long total, List<CartShopSqlView> carts)
        {
            var result = false;

            if (carts.Count > 0)
            {
                using (var db = new ConnectionDB())
                {
                    string query = @"Insert into Invoice(UserID, Total, Address, Date, StatusID)
                                 Values(@0, @1, @2, @3, @4)
                                 Select @@@Identity";

                    int invoiceId = db.ExecuteScalar<int>(query, userId, total, address, DateTime.Now, 1);

                    query = @"Insert into InvoiceDetail(InvoiceID, ProductID, Price, Quantity)
                          Values(@0, @1, @2, @3)

                          Update Product
                          Set Quantity = Quantity - @3
                          Where ProductID = @1";

                    foreach (var item in carts)
                    {
                        var remain = item.RealQuantity - item.Quantity;
                        db.Execute(query, invoiceId, item.ProductId, item.Price, remain > 0 ? item.Quantity : remain);
                    }

                    query = @"Delete From CartShop
                          Where UserID = @0";

                    db.Execute(query, userId);

                    result = true;
                }
            }

            return result;
        }
    }
}
