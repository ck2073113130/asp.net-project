﻿using Business.ViewModels;
using Connection;
using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Business
{
    public class ManagerViewModelsBUS
    {
        public static Page<InvoiceDetailViewModel> GetHistory(string UserID, int page)
        {
            Page<InvoiceDetailViewModel> result;

            using (var db = new ConnectionDB())
            {
                string query = @"Select Inv.InvoiceID, Inv.Date, P.ProductName, InD.Price, S.StatusName, InD.Quantity
                                    From Invoice Inv, Status S, [User] U, InvoiceDetail InD, Product P
                                    Where Inv.StatusID = S.StatusID And
                                            U.Id = Inv.UserID And
                                            InD.InvoiceID = Inv.InvoiceID And
                                            P.ProductID = InD.ProductID And
                                            Inv.UserID = @0
                                    Order By Date Desc";

                result = db.Page<InvoiceDetailViewModel>(page, 10, query, UserID);
            }

            return result;
        }

        public static IEnumerable<InvoiceDetailViewModel> LoadInvoiceDetailsByInvoiceID(int InvoiceID)
        {
            var db = new ConnectionDB();
            string queryString = @"Select InD.InvoiceDetailID, InD.InvoiceID, InD.ProductID, P.ProductName, InD.Price, InD.Quantity
                                    From InvoiceDetail InD, Product P
                                    Where InD.ProductID = P.ProductID And
                                            InD.InvoiceID = @0";

            var list = db.Query<InvoiceDetailViewModel>(queryString, InvoiceID);

            return list;
        }

        public static Page<InvoiceViewModel> GetInvoiceByUserId(int? page, string userId)
        {
            Page<InvoiceViewModel> result;

            using (var db = new ConnectionDB())
            {
                string query = @"Select Date, Total, I.Address, S.StatusName, U.Name, I.InvoiceID
                                 From Invoice I, [User] U, Status S
                                 Where UserID = @0 AND U.Id = UserID AND S.StatusID = I.StatusID";

                result = db.Page<InvoiceViewModel>(page.HasValue ? page.Value : 1, 10,query, userId);

                foreach(var item in result.Items)
                {
                    item.InvoiceDetails = LoadInvoiceDetailsByInvoiceID(item.InvoiceID).ToList();
                }
            }

            return result;
        }
    }
}
