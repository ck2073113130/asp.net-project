﻿using Business.ViewModels;
using Connection;
using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Business
{
    public class ProducerViewModelsBUS
    {
        /// <summary>
        /// Gets Producer List
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<ProducerViewModels> GetProducers()
        {
            IEnumerable<ProducerViewModels> result;

            using (var db = new ConnectionDB())
            {
                string query = @"Select ProducerID, ProducerName, RouteName From Producer Where IsRemoved = @0 Order by ProducerName";

                result = db.Query<ProducerViewModels>(query, 0);
            }

            return result;
        }

        public static Page<ProducerViewModels> GetProducersByProducer(int? page)
        {
            Page<ProducerViewModels> result;

            using (var db = new ConnectionDB())
            {
                string query = @"Select ProducerID, ProducerName, RouteName, Logo From Producer Where IsRemoved = 0 Order by ProducerName";

                result = db.Page<ProducerViewModels>(page.HasValue ? page.Value : 1, 9, query);

            }

            return result;
        }


        /// <summary>
        /// Gets ProducerName by Id
        /// </summary>
        /// <param name="id">Producer Id</param>
        /// <returns></returns>
        public static ProducerViewModels GetProducerNameById(int id)
        {
            ProducerViewModels result;

            using (var db = new ConnectionDB())
            {
                result = db.SingleOrDefault<ProducerViewModels>(@"Select ProducerID, ProducerName, RouteName
                                                                  From Producer
                                                                  Where ProducerID = @0", id);
            }

            return result;
        }
    }
}
