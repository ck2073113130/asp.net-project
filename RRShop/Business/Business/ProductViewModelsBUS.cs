﻿using Business.ViewModels;
using Connection;
using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Business
{
    public class ProductViewModelsBUS
    {
        /// <summary>
        /// Gets Top 16 Sold Products
        /// </summary>
        /// <returns></returns>
        public static List<ProductViewModels> GetTopSellerProducts()
        {
            List<ProductViewModels> result;

            using (var db = new ConnectionDB())
            {
                result = db.Query<ProductViewModels>(
                            @"Select Top(16) P.ProductID, ProductName, Price, RouteName, Quantity, Price - Price * D.Discount as SalePrice 
                              From Product P LEFT JOIN SaleDetail DS LEFT JOIN Discount D ON DS.DiscountID = D.DiscountID LEFT JOIN Sale S ON DS.SaleID = S.SaleID ON P.ProductID = DS.ProductID
                              Where ((S.StartDate is null AND S.DueDate is null) 
                                    OR (GETDATE() >= S.StartDate AND GETDATE() <= S.DueDate))
                                    AND IsRemoved = @0
                              Order by P.Sold DESC, P.ProductID", 0)
                           .ToList();

                foreach (var element in result)
                {
                    element.Images = ImageBUS.GetImageByProduct(element.ProductID);
                }
            }

            return result;
        }

        /// <summary>
        /// Gets Top 16 New Products
        /// </summary>
        /// <returns></returns>
        public static List<ProductViewModels> GetTopNewProducts()
        {
            List<ProductViewModels> result;

            using (var db = new ConnectionDB())
            {
                result = db.Query<ProductViewModels>(
                            @"Select Top(16) P.ProductID, ProductName, Price, RouteName, Quantity, Price - Price * D.Discount as SalePrice 
                              From Product P LEFT JOIN SaleDetail DS LEFT JOIN Discount D ON DS.DiscountID = D.DiscountID LEFT JOIN Sale S ON DS.SaleID = S.SaleID ON P.ProductID = DS.ProductID
                              Where ((S.StartDate is null AND S.DueDate is null) 
                                    OR (GETDATE() >= S.StartDate AND GETDATE() <= S.DueDate))
                                    AND IsRemoved = @0
                              Order by P.UploadDate DESC, P.ProductID", 0)
                           .ToList();

                foreach (var element in result)
                {
                    element.Images = ImageBUS.GetImageByProduct(element.ProductID);
                }
            }

            return result;
        }

        /// <summary>
        /// Gets Top 16 Saled Products
        /// </summary>
        /// <returns></returns>
        public static List<ProductViewModels> GetTopSaledProducts()
        {
            List<ProductViewModels> result;

            using (var db = new ConnectionDB())
            {
                result = db.Query<ProductViewModels>(
                            @"Select Top(16) P.ProductID, ProductName, Price, RouteName, Quantity, Price - Price * D.Discount as SalePrice 
                              From Product P LEFT JOIN SaleDetail DS LEFT JOIN Discount D ON DS.DiscountID = D.DiscountID LEFT JOIN Sale S ON DS.SaleID = S.SaleID ON P.ProductID = DS.ProductID
                              Where ((S.StartDate is null AND S.DueDate is null) 
                                    OR (GETDATE() >= S.StartDate AND GETDATE() <= S.DueDate))
                                    AND IsRemoved = @0
                              Order by SalePrice DESC, P.ProductID", 0)
                           .ToList();

                foreach (var element in result)
                {
                    element.Images = ImageBUS.GetImageByProduct(element.ProductID);
                }
            }

            return result;
        }

        /// <summary>
        /// Gets Top 16 Viewed Products
        /// </summary>
        /// <returns></returns>
        public static List<ProductViewModels> GetTopViewedProducts()
        {
            List<ProductViewModels> result;

            using (var db = new ConnectionDB())
            {
                result = db.Query<ProductViewModels>(
                            @"Select Top(16) P.ProductID, ProductName, Price, RouteName, Quantity, Price - Price * D.Discount as SalePrice 
                              From Product P LEFT JOIN SaleDetail DS LEFT JOIN Discount D ON DS.DiscountID = D.DiscountID LEFT JOIN Sale S ON DS.SaleID = S.SaleID ON P.ProductID = DS.ProductID
                              Where ((S.StartDate is null AND S.DueDate is null) 
                                    OR (GETDATE() >= S.StartDate AND GETDATE() <= S.DueDate))
                                    AND IsRemoved = @0
                              Order by P.Viewed DESC, P.ProductID", 0)
                           .ToList();

                foreach (var element in result)
                {
                    element.Images = ImageBUS.GetImageByProduct(element.ProductID);
                }
            }

            return result;
        }

        /// <summary>
        /// Gets Product List based on Producer
        /// </summary>
        /// <param name="id">Producer ID</param>
        /// <returns></returns>
        public static Page<ProductViewModels> GetProductsByProducer(int id, int? page)
        {
            Page<ProductViewModels> result;

            using (var db = new ConnectionDB())
            {
                string query = @"Select P.ProductID, ProductName, Price, RouteName, Quantity, Price - Price * D.Discount as SalePrice 
                                 From Product P LEFT JOIN SaleDetail DS LEFT JOIN Discount D ON DS.DiscountID = D.DiscountID LEFT JOIN Sale S ON DS.SaleID = S.SaleID ON P.ProductID = DS.ProductID
                                 Where ((S.StartDate is null AND S.DueDate is null) 
                                       OR (GETDATE() >= S.StartDate AND GETDATE() <= S.DueDate))
                                       AND P.ProducerID = @0
                                       AND IsRemoved = 0";

                result = db.Page<ProductViewModels>(page.HasValue ? page.Value : 1, 9, query, id);

                foreach (var element in result.Items)
                {
                    element.Images = ImageBUS.GetImageByProduct(element.ProductID);
                }
            }

            return result;
        }

        /// <summary>
        /// Gets Product List based on Category
        /// </summary>
        /// <param name="id">Category ID</param>
        /// <returns></returns>
        public static Page<ProductViewModels> GetProductsByCategory(int id, int? page)
        {
            Page<ProductViewModels> result;

            using (var db = new ConnectionDB())
            {
                string query = @"Select P.ProductID, ProductName, Price, RouteName, Quantity, Price - Price * D.Discount as SalePrice 
                                 From Product P LEFT JOIN SaleDetail DS LEFT JOIN Discount D ON DS.DiscountID = D.DiscountID LEFT JOIN Sale S ON DS.SaleID = S.SaleID ON P.ProductID = DS.ProductID
                                 Where ((S.StartDate is null AND S.DueDate is null) 
                                       OR (GETDATE() >= S.StartDate AND GETDATE() <= S.DueDate))
                                       AND P.CategoryID = @0
                                       AND IsRemoved = @1";

                result = db.Page<ProductViewModels>(page.HasValue ? page.Value : 1, 9, query, id, 0);

                foreach (var element in result.Items)
                {
                    element.Images = ImageBUS.GetImageByProduct(element.ProductID);
                }
            }

            return result;
        }

        /// <summary>
        /// Gets Product List based on Sub Category
        /// </summary>
        /// <param name="id">Category ID</param>
        /// <returns></returns>
        public static Page<ProductViewModels> GetProductsBySubCategory(int id, int? page)
        {
            Page<ProductViewModels> result;

            using (var db = new ConnectionDB())
            {
                string query = @"Select P.ProductID, ProductName, Price, RouteName, Quantity, Price - Price * D.Discount as SalePrice 
                                 From Product P LEFT JOIN SaleDetail DS LEFT JOIN Discount D ON DS.DiscountID = D.DiscountID LEFT JOIN Sale S ON DS.SaleID = S.SaleID ON P.ProductID = DS.ProductID
                                 Where ((S.StartDate is null AND S.DueDate is null) 
                                       OR (GETDATE() >= S.StartDate AND GETDATE() <= S.DueDate))
                                       AND P.SubCategoryID = @0 AND P.IsRemoved = 0";

                result = db.Page<ProductViewModels>(page.HasValue ? page.Value : 1, 9, query, id);

                foreach (var element in result.Items)
                {
                    element.Images = ImageBUS.GetImageByProduct(element.ProductID);
                }
            }

            return result;
        }

        /// <summary>
        /// Gets Saled Product List
        /// </summary>
        /// <returns></returns>
        public static Page<ProductViewModels> GetSaledProducts(int? page)
        {
            Page<ProductViewModels> result;

            using (var db = new ConnectionDB())
            {
                string query = @"Select P.ProductID, ProductName, Price, RouteName, Quantity, Price - Price * D.Discount as SalePrice 
                                 From Product P LEFT JOIN SaleDetail DS LEFT JOIN Discount D ON DS.DiscountID = D.DiscountID LEFT JOIN Sale S ON DS.SaleID = S.SaleID ON P.ProductID = DS.ProductID
                                 Where GETDATE() >= S.StartDate AND GETDATE() <= S.DueDate
                                       AND IsRemoved = 0";

                result = db.Page<ProductViewModels>(page.HasValue ? page.Value : 1, 9, query);

                foreach (var element in result.Items)
                {
                    element.Images = ImageBUS.GetImageByProduct(element.ProductID);
                }
            }

            return result;
        }

        /// <summary>
        /// Gets 8 Related Products with similar Category
        /// </summary>
        /// <param name="id">ID of current Product</param>
        /// <returns></returns>
        public static List<ProductViewModels> GetRelatedCategoryProducts(byte id, int productID)
        {
            List<ProductViewModels> result;

            using (var db = new ConnectionDB())
            {
                result = db.Query<ProductViewModels>(@"Select   Top 8 P.ProductID, ProductName, Price, Price - Price * D.Discount as SalePrice, RouteName, Quantity
                                                       From     Product P LEFT JOIN SaleDetail DS LEFT JOIN Discount D ON DS.DiscountID = D.DiscountID LEFT JOIN Sale S ON DS.SaleID = S.SaleID ON P.ProductID = DS.ProductID
                                                       Where    ((S.StartDate is null AND S.DueDate is null) 
                                                                OR (GETDATE() >= S.StartDate AND GETDATE() <= S.DueDate))
                                                                AND P.CategoryID = @0
                                                                AND P.ProductID != @1
                                                                AND IsRemoved = @2
                                                       Order By NEWID()", id, productID, 0)
                           .ToList();

                foreach (var element in result)
                {
                    element.Images = ImageBUS.GetImageByProduct(element.ProductID);
                }
            }

            return result;
        }

        /// <summary>
        /// Gets 8 Related Products with similar Sub Category 
        /// </summary>
        /// <param name="id">ID of current Product</param>
        /// <returns></returns>
        public static List<ProductViewModels> GetRelatedSubCategoryProducts(int id)
        {
            return null;
        }

        /// <summary>
        /// Gets 8 Related Products with similar Producer
        /// </summary>
        /// <param name="id">ID of current Product</param>
        /// <returns></returns>
        public static List<ProductViewModels> GetRelatedProducerProducts(short id, int productID)
        {
            List<ProductViewModels> result;

            using (var db = new ConnectionDB())
            {
                result = db.Query<ProductViewModels>(@"Select   Top 8 P.ProductID, ProductName, Price, Price - Price * D.Discount as SalePrice, RouteName, Quantity
                                                       From     Product P LEFT JOIN SaleDetail DS LEFT JOIN Discount D ON DS.DiscountID = D.DiscountID LEFT JOIN Sale S ON DS.SaleID = S.SaleID ON P.ProductID = DS.ProductID
                                                       Where    ((S.StartDate is null AND S.DueDate is null) 
                                                                OR (GETDATE() >= S.StartDate AND GETDATE() <= S.DueDate))
                                                                AND P.ProducerID = @0
                                                                AND P.ProductID != @1
                                                                AND IsRemoved = @2
                                                       Order By NEWID()", id, productID, 0)
                           .ToList();

                foreach (var element in result)
                {
                    element.Images = ImageBUS.GetImageByProduct(element.ProductID);
                }
            }

            return result;
        }

        public static ProductViewModels GetProductById(int id)
        {
            ProductViewModels result;

            using (var db = new ConnectionDB())
            {
                result = db.SingleOrDefault<ProductViewModels>(@"Select P.ProductID, ProductName, Price, Quantity, Price - Price * D.Discount as SalePrice, 
                                                                        P.Description, P.CategoryID, P.ProducerID, CategoryName, SubCategoryName, P.SubCategoryID,
                                                                        C.RouteName as CategoryRouteName, SC.RouteName as SubCategoryRouteName, P.Viewed
                                                                 From   Product P LEFT JOIN SubCategory SC ON P.SubCategoryID = SC.SubCategoryID LEFT JOIN SaleDetail DS LEFT JOIN Discount D ON DS.DiscountID = D.DiscountID LEFT JOIN Sale S ON DS.SaleID = S.SaleID ON P.ProductID = DS.ProductID,
                                                                        Category C
                                                                 Where  ((S.StartDate is null AND S.DueDate is null) 
                                                                        OR (GETDATE() >= S.StartDate AND GETDATE() <= S.DueDate))
                                                                        AND P.CategoryID = C.CategoryID
                                                                        AND P.ProductID = @0", id);

                result.Images = ImageBUS.GetImagesByProduct(id);
            }

            return result;
        }

        public static List<ProductViewModels> GetHotSaleProducts()
        {
            List<ProductViewModels> result;

            using (var db = new ConnectionDB())
            {
                result = db.Query<ProductViewModels>(
                            @"Select Top(3) P.ProductID, ProductName, Price, RouteName, Quantity, Price - Price * D.Discount as SalePrice 
                              From Product P, SaleDetail DS, Discount D, Sale S
                              Where P.ProductID = DS.ProductID
									AND S.SaleID = DS.SaleID
									AND DS.DiscountID = D.DiscountID
									AND P.Saled = @0
                                    AND IsRemoved = @1
                              Order by P.Sold DESC, P.ProductID", 1, 0)
                           .ToList();

                foreach (var element in result)
                {
                    element.Images = ImageBUS.GetImageByProduct(element.ProductID);
                }
            }

            return result;
        }

        public static void UpdateViewedForProduct(int proId)
        {
            using (var db = new ConnectionDB())
            {
                string query = @"Update Product
                                 Set Viewed = Viewed + 1
                                 Where ProductID = @0";
                db.Execute(query, proId);
            }
        }
    }
}
