﻿using Business.ViewModels;
using Connection;
using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Business
{
    public class Searching
    {
        public static Page<ProductViewModels> NormalSearch(string keyword, int? page)
        {
            Page<ProductViewModels> result;
            keyword = keyword.ToUnsign();
            keyword = "%" + keyword + "%";
            using (var db = new ConnectionDB())
            {
                string query = @"Select P.ProductID, ProductName, Price, P.RouteName, Quantity, Price - Price * D.Discount as SalePrice 
                                    From Producer PC, Product P LEFT JOIN SaleDetail DS 
		                                    LEFT JOIN Discount D ON DS.DiscountID = D.DiscountID 
		                                    LEFT JOIN Sale S ON DS.SaleID = S.SaleID ON P.ProductID = DS.ProductID
		                                    LEFT JOIN Category cat on p.CategoryID = cat.CategoryID
		                                    LEFT JOIN SubCategory SUB ON SUB.CategoryID = cat.CategoryID and sub.SubCategoryID = p.SubCategoryID and SUB.IsRemoved = 0 and cat.IsRemoved = 0
                                    Where ((S.StartDate is null AND S.DueDate is null) 
		                                    OR (GETDATE() >= S.StartDate AND GETDATE() <= S.DueDate)) AND
		                                    P.ProducerID = PC.ProducerID AND
                                            P.IsRemoved = 0 AND PC.IsRemoved = 0 AND (
		                                    SUB.RouteName like @0 OR
		                                    cat.RouteName like @0 OR
                                            PC.RouteName like @0 OR
		                                    P.RouteName like @0) ";

                result = db.Page<ProductViewModels>(page.HasValue ? page.Value : 1, 9, query, keyword);

                foreach (var element in result.Items)
                {
                    element.Images = ImageBUS.GetImageByProduct(element.ProductID);
                }
            }

            return result;
        }

        public static Page<ProductViewModels> AdvancedSearch(SearchItems item, int? page)
        {
            Page<ProductViewModels> result;

            StringBuilder query = new StringBuilder(@"Select P.ProductID, ProductName, Price, P.RouteName, Quantity, Price - Price * D.Discount as SalePrice 
                                                        From Producer PC, Product P LEFT JOIN SaleDetail DS 
		                                                        LEFT JOIN Discount D ON DS.DiscountID = D.DiscountID 
		                                                        LEFT JOIN Sale S ON DS.SaleID = S.SaleID ON P.ProductID = DS.ProductID
		                                                        LEFT JOIN Category cat on p.CategoryID = cat.CategoryID
		                                                        LEFT JOIN SubCategory SUB ON SUB.CategoryID = cat.CategoryID and sub.SubCategoryID = p.SubCategoryID and SUB.IsRemoved = 0 and cat.IsRemoved = 0
                                                        Where ((S.StartDate is null AND S.DueDate is null) 
		                                                        OR (GETDATE() >= S.StartDate AND GETDATE() <= S.DueDate)) AND
		                                                        P.ProducerID = PC.ProducerID AND
                                                                P.IsRemoved = 0 AND PC.IsRemoved = 0 ");

            if(item.Keyword != null)
            {
                item.Keyword = item.Keyword.ToUnsign();
                //item.Keyword = " %" + item.Keyword + "%";
                query.Append(" AND P.RouteName like '%" + item.Keyword + "%' ");
            }

            if(item.CategoryID != -1)
            {
                query.Append(" AND cat.CategoryID = " + item.CategoryID);
            }

            if(item.SubCategoryID != -1)
            {
                query.Append(" AND SUB.SubCategoryID = " + item.SubCategoryID);
            }

            if(item.ProducerID != -1)
            {
                query.Append(" AND PC.ProducerID = " + item.ProducerID);
            }

            if(item.PriceTo != 0)
            {
                query.Append(" AND Price >= " + item.PriceFrom + " AND Price <= " + item.PriceTo);
            }

            using (var db = new ConnectionDB())
            {
                result = db.Page<ProductViewModels>(page.HasValue ? page.Value : 1, 9, query.ToString());

                foreach (var element in result.Items)
                {
                    element.Images = ImageBUS.GetImageByProduct(element.ProductID);
                }
            }

            return result;
        }
    }
}
