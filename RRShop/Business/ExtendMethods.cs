﻿using System;
using System.Text;
using System.Text.RegularExpressions;

namespace Business
{
    public static class ExtendMethods
    {
        public static string ToUnsign(this string value)
        {
            Regex regex = new Regex("\\p{IsCombiningDiacriticalMarks}+");
            string temp = value.Normalize(NormalizationForm.FormD);
            return regex.Replace(temp, String.Empty)
                .Replace('\u0111', 'd')
                .Replace('\u0110', 'D')
                .Replace(' ', '-')
                .Replace(@"[^A-Za-z0-9.-\s]+", "");
        }
    }
}
