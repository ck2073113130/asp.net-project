﻿using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.ViewModels
{
    public class CategoryViewModels
    {
        [Column]
        public byte CategoryID { get; set; }

        [Column]
        public string CategoryName { get; set; }

        [Column]
        public string Image { get; set; }

        [Column]
        public string RouteName { get; set; }

        [Column]
        public string Banner { get; set; }
    }
}
