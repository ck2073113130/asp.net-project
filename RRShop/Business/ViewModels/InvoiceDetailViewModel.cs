﻿using Connection;
using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.ViewModels
{
    public class InvoiceDetailViewModel : InvoiceDetail
    {
        [Column] public string ProductName { get; set; }
        [Column] public short StatusID { get; set; }
        [Column] public string StatusName { get; set; }
        [Column]
        public DateTime Date { get; set; }
        [Column]
        public long Total { get; set; }
    }
}
