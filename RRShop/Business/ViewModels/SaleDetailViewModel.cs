﻿using Connection;
using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.ViewModels
{
    public class SaleDetailViewModel : SaleDetail
    {
        [Column]
        public string ProductName { get; set; }
        [Column]
        public string Value { get; set; }
        [Column]
        public double Discount { get; set; }
        [Column]
        public int? Price { get; set; }
        
    }
}
