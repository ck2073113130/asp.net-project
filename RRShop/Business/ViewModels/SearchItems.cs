﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.ViewModels
{
    public class SearchItems
    {
        public string Keyword { get; set; }

        public short CategoryID { get; set; }

        public short SubCategoryID { get; set; }

        public short ProducerID { get; set; }

        public int PriceFrom { get; set; }

        public int PriceTo { get; set; }
    }
}
