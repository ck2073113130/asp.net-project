﻿using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.ViewModels
{
    public class StatisticCatViewModel
    {
        [Column] public string ProducerName { get; set; }
        [Column] public string CategoryName { get; set; }
        [Column] public int Quantity { get; set; }
    }
}
