﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.ViewModels
{
    public class UserViewModel
    {
        public string Id { get; set; }

        public string Email { get; set; }

        [Required(ErrorMessage = "Tên người dùng không được rỗng")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Địa chỉ không được rỗng")]
        public string Address { get; set; }

        [RegularExpression(@"\d*$", ErrorMessage = "Số chứng minh thư chỉ tồn tại chữ số")]
        [StringLength(11, ErrorMessage = "Số chứng minh thư từ 9 - 11 số", MinimumLength = 9)]
        [Required(ErrorMessage = "Chứng minh thư không được rỗng")]
        public string PersonalIdentity { get; set; }

        [Required(ErrorMessage = "Số điện thoại không được rỗng")]
        [RegularExpression(@"^(09\d{8})|(088\d{7})|(01\d{9})$", ErrorMessage = "Số điện thoại không đúng định dạng")]
        public string PhoneNumber { get; set; }

        public DateTime DateOfBirth { get; set; }

        public bool Gender { get; set; }

        public bool IsRemoved { get; set; }
    }
}
