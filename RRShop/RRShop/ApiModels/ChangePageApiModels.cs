﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RRShop.ApiModels
{
    public class ChangePageApiModels
    {
        public int Page { get; set; }

        public int ProductId { get; set; }
    }
}