﻿using Business.AdminBUS;
using Business.Business;
using Business.ViewModels;
using Connection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace RRShop.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class ListController : Controller
    {
        public ActionResult CategoryList(int? CatID)
        {
            if (!CatID.HasValue)
            {
                CatID = 1;
            }
            Category cat = new Category();
            cat.CategoryID = (byte)CatID.Value;

            IEnumerable<Category> IlistCat = CategoryBUS.GetCategories();
            List<SelectListItem> listCat = new List<SelectListItem>();

            listCat.Add(new SelectListItem
            {
                Text = "-- Chọn loại --",
                Value = "-1"
            });

            foreach (var items in IlistCat)
            {
                listCat.Add(new SelectListItem
                {
                    Text = items.CategoryName,
                    Value = items.CategoryID.ToString(),
                    Selected = items.CategoryID == CatID ? true : false
                });
            }
            ViewBag.listCat = listCat;

            return View(cat);
        }

        public ActionResult SubCategoryList(int? SubCatID)
        {
            if (!SubCatID.HasValue)
            {
                SubCatID = 1;
            }
            SubCategory sub = new SubCategory();
            sub.SubCategoryID = (byte)SubCatID.Value;

            IEnumerable<SubCategoryViewModels> IlistSubCat = SubCategoryAdmin.GetSubCategoriesBySubCategory(SubCatID.Value);
            List<SelectListItem> listSubCat = new List<SelectListItem>();



            foreach (var items in IlistSubCat)
            {
                listSubCat.Add(new SelectListItem
                {
                    Text = items.SubCategoryName,
                    Value = items.SubCategoryID.ToString(),
                    Selected = items.SubCategoryID == SubCatID ? true : false
                });
            }
            ViewBag.listSubCat = listSubCat;

            return View(sub);
        }

        public ActionResult ProducerList(int? ProducerID)
        {
            if (!ProducerID.HasValue)
            {
                ProducerID = 1;
            }
            Producer producer = new Producer();
            producer.ProducerID = (short)ProducerID.Value;

            IEnumerable<ProducerViewModels> IlistProducer = ProducerViewModelsBUS.GetProducers();
            List<SelectListItem> listProducer = new List<SelectListItem>();

            listProducer.Add(new SelectListItem
            {
                Text = "-- Chọn NSX --",
                Value = "-1"
            });

            foreach (var items in IlistProducer)
            {
                listProducer.Add(new SelectListItem
                {
                    Text = items.ProducerName,
                    Value = items.ProducerID.ToString(),
                    Selected = items.ProducerID == ProducerID ? true : false
                });
            }
            ViewBag.listProducer = listProducer;
            return View(producer);
        }
    }
}