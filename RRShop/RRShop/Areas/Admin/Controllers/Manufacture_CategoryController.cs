﻿using Business.AdminBUS;
using Connection;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RRShop.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class Manufacture_CategoryController : Controller
    {
        // GET: Admin/Manufacture_Category
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult CategoryView()
        {
            return View(CategoryAdmin.LoadAllCategory());
        }

        public ActionResult SubCategoryView()
        {
            return View(SubCategoryAdmin.LoadAllSubCategory());
        }

        public ActionResult ProducerView()
        {
            return View(ProducerAdmin.LoadAllProducer());
        }

        //-----------------------------First line category---------------------------------
        public ActionResult UpdateCategory(Category cat)
        {
            return Json(CategoryAdmin.UpdateCategory(cat), JsonRequestBehavior.AllowGet);
        }

        public ActionResult UploadBannerCategory(int CatID)
        {
            Category cat = CategoryAdmin.GetCategoryByCatID(CatID);
            string fullPath = Request.MapPath("~" + cat.Banner);
            if (System.IO.File.Exists(fullPath))
            {
                System.IO.File.Delete(fullPath);
            }

            string path = Server.MapPath("~/images/banner");
            string relativePath = "/images/banner";

            HttpPostedFileBase file = Request.Files[0];
            if (file.ContentLength > 0)
            {
                string extension = Path.GetExtension(file.FileName);
                string fileName = cat.RouteName + extension;

                cat.Banner = relativePath + "/" + fileName;
                CategoryAdmin.UpdateBannerCategory(cat);

                var thisPath = Path.Combine(path, fileName);
                file.SaveAs(thisPath);
            }
            return Json(cat.Banner, JsonRequestBehavior.AllowGet);
        }

        public ActionResult InsertCategory(string CategoryName)
        {
            Category cat = new Category();
            cat.CategoryName = CategoryName;
            return Json(CategoryAdmin.AddCategory(cat), JsonRequestBehavior.AllowGet);
        }

        public ActionResult DeleteCategory(int CategoryID)
        {
            return Json(CategoryAdmin.DeleteCategory(CategoryID), JsonRequestBehavior.AllowGet);
        }
        //-----------------------------Last line category---------------------------------


        //-----------------------------First line producer--------------------------------
        public ActionResult UpdateProducer(Producer producer)
        {
            return Json(ProducerAdmin.UpdateProducer(producer), JsonRequestBehavior.AllowGet);
        }

        public ActionResult DeleteProducer(int ProducerID)
        {
            return Json(ProducerAdmin.DeleteProducer(ProducerID), JsonRequestBehavior.AllowGet);
        }

        public ActionResult InsertProducer(string ProducerName)
        {
            Producer producer = new Producer();
            producer.ProducerName = ProducerName;
            return Json(ProducerAdmin.AddProducer(producer), JsonRequestBehavior.AllowGet);
        }

        public ActionResult UploadLogoProducer(int ProducerID)
        {
            //Category cat = CategoryAdmin.GetCategoryByCatID(CatID);
            Producer producer = ProducerAdmin.GetProducerByProducerID(ProducerID);
            string fullPath = Request.MapPath("~" + producer.Logo);
            if (System.IO.File.Exists(fullPath))
            {
                System.IO.File.Delete(fullPath);
            }

            string path = Server.MapPath("~/images/logo");
            string relativePath = "/images/logo";

            HttpPostedFileBase file = Request.Files[0];
            if (file.ContentLength > 0)
            {
                string extension = Path.GetExtension(file.FileName);
                string fileName = producer.RouteName + extension;

                producer.Logo = relativePath + "/" + fileName;
                ProducerAdmin.UpdateLogoProducer(producer);

                var thisPath = Path.Combine(path, fileName);
                file.SaveAs(thisPath);
            }
            return Json(producer.Logo, JsonRequestBehavior.AllowGet);
        }
        //-----------------------------Last line producer---------------------------------

        //-----------------------------First line SubCategory--------------------------------
        public ActionResult UpdateSubCategory(SubCategory sub)
        {
            return Json(SubCategoryAdmin.UpdateSubCategory(sub), JsonRequestBehavior.AllowGet);
        }

        public ActionResult DeleteSubCategory(int SubCategoryID)
        {
            return Json(SubCategoryAdmin.DeleteSubCategory(SubCategoryID), JsonRequestBehavior.AllowGet);
        }

        public ActionResult InsertSubCategory(string SubCategoryName, int CategoryID)
        {
            SubCategory sub = new SubCategory();
            sub.SubCategoryName = SubCategoryName;
            sub.CategoryID = (byte)CategoryID;
            return Json(SubCategoryAdmin.AddSubCategory(sub), JsonRequestBehavior.AllowGet);
        }
        //-----------------------------Last line SubCategory---------------------------------
    }
}