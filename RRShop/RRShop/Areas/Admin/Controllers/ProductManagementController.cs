﻿using Business.AdminBUS;
using Business.ViewModels;
using Connection;
using PetaPoco;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RRShop.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class ProductManagementController : Controller
    {

        // GET: Admin/ProductManagement
        public ActionResult Index(short? id)
        {
            if (!id.HasValue)
            {
                id = 1;
            }
            return View(ProductAdmin.LoadProductByPage(id.Value));
        }

        public ActionResult Detail(short? ProID)
        {
            if (!ProID.HasValue)
            {
                ProID = 0;
            }
            return View(ProductAdmin.GetProductByID(ProID.Value));
        }

        [ValidateInput(false)]
        public ActionResult UpdateProduct(Product pro)
        {
            ProductAdmin.UpdateProduct(pro);
            return RedirectToAction("Detail", "ProductManagement", new { ProID = pro.ProductID});
        }

        public ActionResult Gallery(short? ProID)
        {
            FileUploadContainer fileUpload = new FileUploadContainer();
            if (!ProID.HasValue)
            { 
                ProID = 0;
            }

            ViewBag.ProName = ProductAdmin.GetProductNameByID(ProID.Value);
            fileUpload.ProductID = ProID.Value;
            return View(fileUpload);
        }

        [HttpPost]
        public ActionResult UploadImages(FileUploadContainer file)
        {
            if (file.Files.First() == null)
            {
                return RedirectToAction("ManageGallery/" + file.ProductID);
            }
            string path = Server.MapPath("~/images/product" + "/" + file.ProductID);
            string relativePath = "/images/product" + "/" + file.ProductID;
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            foreach (var item in file.Files)
            {
                if (item.ContentLength > 0)
                {
                    Image img = new Image();
                    img.ProductID = file.ProductID;
                    img.Link = relativePath + "/" + item.FileName;
                    img.IsMainImage = false;
                    ImageAdmin.InsertImage(img);
                    

                    var fileName = Path.GetFileName(item.FileName);
                    var thisPath = Path.Combine(path, fileName);
                    item.SaveAs(thisPath);
                }
            }

            return RedirectToAction("Gallery", new { ProID = file.ProductID });
        }

        public ActionResult ShowUploaded(short? ProID)
        {
            if (!ProID.HasValue)
            {
                ProID = 0;
            }
           
            return View(ImageAdmin.GetImageByProID(ProID.Value));
        }

        public ActionResult DeleteImg(Image img)
        {

            ImageAdmin.DeleteImage(img);
            string path = Server.MapPath(img.Link);
            if(path != null)
            {
                System.IO.File.Delete(path);
            }
            return RedirectToAction("Gallery", new { ProID = img.ProductID });
        }

        public ActionResult SetMainImg(Image img)
        {
            ImageAdmin.SetAllFalse(img.ProductID.Value);
            ImageAdmin.SetMainImg(img.ImageID);
            return RedirectToAction("Gallery", new { ProID = img.ProductID });
        }

        public ActionResult FormCreateProduct()
        {
            ProductViewModelForAdmin pro = new ProductViewModelForAdmin();
            return View(pro);
        }

        [ValidateInput(false)]
        public ActionResult CreateProduct(Product pro)
        {
            ProductAdmin.AddProduct(pro);
            return  RedirectToAction("FormCreateProduct");
        }

        public ActionResult AjaxGetSub(int CatID)
        {
            return Json(SubCategoryAdmin.GetSubCategoryByCategory(CatID), JsonRequestBehavior.AllowGet);
        }

        public ActionResult DeleteProduct(int ProID)
        {
            return Json(ProductAdmin.DeleteProduct(ProID), JsonRequestBehavior.AllowGet);
        }

    }
}