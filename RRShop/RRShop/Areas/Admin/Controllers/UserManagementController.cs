﻿using Business.AdminBUS;
using Business.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RRShop.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class UserManagementController : Controller
    {
        // GET: Admin/UserManagement
        public ActionResult Index(short? page)
        {
            if (!page.HasValue)
            {
                page = 1;
            }
            return View(UserAdmin.GetAllUsers(page.Value));
        }

        public ActionResult Detail(string Id)
        {
            if (Id == null)
            {
                Id = "";
            }

            return View(UserAdmin.GetUserById(Id));
        }

        public ActionResult DeleteUser(string UserID)
        {
            return Json(UserAdmin.DeleteUser(UserID), JsonRequestBehavior.AllowGet);
        }

        public ActionResult UpdateUser(UserViewModel user, string[] RoleID)
        {
            if (Request["Gender"] == "Nam")
            {
                user.Gender = true;
            }
            else
            {
                user.Gender = false;
            }

            UserRoleAdmin.DeleteAllUserRoleByUserId(user.Id);
            if(RoleID != null)
            {
                foreach (var item in RoleID)
                {
                    UserRoleViewModel UserRole = new UserRoleViewModel();
                    UserRole.UserId = user.Id;
                    UserRole.RoleId = item;
                    UserRoleAdmin.InsertUserRole(UserRole);
                }
            }   

            UserAdmin.UpdateUser(user);
            return RedirectToAction("Detail", "UserManagement", new { Id = user.Id }); ;
        }
    }
}