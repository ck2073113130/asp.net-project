﻿using Business.AdminBUS;
using Business.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RRShop.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class UserRoleManamentController : Controller
    {
        // GET: Admin/UserRoleManament
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ShowRole(string Id)
        {
            ViewBag.UserId = Id; 
            return View(UserRoleAdmin.GetRoleByUser(Id));
        }

        public ActionResult UpdateUserRole(string UserID, string RoleID, bool IsAddOrRemove)
        {
            bool result;
            UserRoleViewModel UserRole = new UserRoleViewModel();
            UserRole.UserId = UserID;
            UserRole.RoleId = RoleID;
            if (IsAddOrRemove)
            {
                result = UserRoleAdmin.InsertUserRole(UserRole);
            }
            else
            {
                result = UserRoleAdmin.DeleteUserRole(UserRole);
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }
         
    }
}