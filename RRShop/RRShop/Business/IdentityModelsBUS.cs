﻿using RRShop.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RRShop.Business
{
    public class IdentityModelsBUS
    {
        public static ApplicationUser GetUserById(string userId)
        {
            ApplicationUser result;

            using (var db = new ApplicationDbContext())
            {
                result = db.Users.SingleOrDefault(u => u.Id == userId);
            }

            return result;
        }
    }
}