﻿using Business.Business;
using System.Web.Mvc;

namespace RRShop.Controllers
{
    [RoutePrefix("Cat")]
    public class CategoryController : Controller
    {
        [Route("{id}/{name}/{page?}")]
        public ActionResult Cat(int? page, string name = "", int id = 0)
        {
            if (id == 0 || name == "")
            {
                return RedirectToAction("Index", "Home");
            }

            ViewBag.Category = CategoryViewModelsBUS.GetCategoriesWithBanner(id);

            return View(ProductViewModelsBUS.GetProductsByCategory(id, page));
        }

        [Route("{id}/{name}/Sub/{subId}/{subname}/{page?}")]
        public ActionResult Sub(int? page, int id = 0, string name = "", byte subId = 0, string subname = "")
        {
            if (id == 0 || subname == "" || name == "" || subId == 0)
            {
                return RedirectToAction("Index", "Home");
            }

            ViewBag.Category = CategoryViewModelsBUS.GetCategoryById(id);

            ViewBag.SubCategoryName = SubCategoryViewModelsBUS.GetSubCategoryNameById(subId);

            ViewBag.SubCategoryID = subId;
            ViewBag.SubCategoryRouteName = subname;

            return View(ProductViewModelsBUS.GetProductsBySubCategory(subId, page));
        }
    }
}