﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RRShop.Controllers
{
    [RoutePrefix("Error")]
    public class ErrorController : Controller
    {
        [Route("404")]
        public ActionResult _404()
        {
            return View();
        }
    }
}