﻿var paths = [];

function hideQVButton(element, ce) {
    $(element + ' .btn-quickview-' + ce).hide();
}

function showQVButton(element, ce) {
    $(element + ' .btn-quickview-' + ce).show();
}

function closeQVFrame() {
    $('div.quickview-container').hide();
    $('#quickview-bg-block').hide();
    $('.quickview-load-img').hide();
    $('div#quickview-content').hide(600);
    $('div#quickview-content').html('');
    $('a.a-qv-close').hide();
}

function appendQuickViewEvent(element, ce) {
    var clickEvent = "ajaxView('" + paths[ce] + "')";
    $(element + ' .btn-quickview-' + ce).attr('onclick', clickEvent);
}

function appendCloseFrameLink() {
    $('div#quickview-content').prepend("<a href='javascript:void(0);' class='a-qv-close' onclick='closeQVFrame()'>Close</a>");
}

function appendQuickViewinListScript() {
    initQuickButton('.category-products');
    $(document).ajaxComplete(function () {
        initQuickButton('.category-products');
    });
}

function initQuickButton(element) {
    var count = 0;
    var subpath = "";
    $(element + ' a.product-image').each(function (path) {
        var rel = $(this).attr('rel');
        if (rel == null || rel == '') {
            count++;
            path = $(this).attr('href');
            subpath = path.substring(path.lastIndexOf('/') + 1, path.length);
            paths[count] = subpath;
            $(this).closest("div.item-inner").children('.images-container').children(".actions").children(".add-to-links")
									.append('<li class="qv-button-container"><a ' + 'class="qv-e-button btn-quickview-' + count + '"><em ' + 'class="tooltip">' + 'Xem nhanh</em>' + '</a></li>');
            appendQuickViewEvent(element, count);
            $(this).attr('rel', 'author');

            $("[class*='btn-quickview-'] > .tooltip").each(function () {
                $(this).css({
                    "margin-left": -$(this).width() / 2
                });
            });
        }
        if (rel == 'author') {
            $(this).attr('rel', '');
        }
    });
}

function ajaxView(path) {

    $('#quickview-bg-block').show();
    $('.quickview-load-img').show();

    $.ajax({
        url: MA.QuickView.BASE_URL + 'quickview/index/view/path/' + path,
        type: "get",
        success: function (response) {


            $('a.a-qv-close').show();
            $('.quickview-load-img').hide();
            $('div#quickview-content').show();
            $('div.quickview-container').show();
            $('div#quickview-content').html(response);
        }
    });
}

$(document).ready(function () {
    if (MA.QuickView.CATEGORY_ENABLE == 'true') {
        $('.category-products').append('<script type="text/javascript">appendQuickViewinListScript()</script>');
    }
});





