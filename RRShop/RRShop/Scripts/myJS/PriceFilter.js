﻿$(function () {
    var currencies = "$";
    var toolbar_status = "1";
    var rate = "1";
    var min = "99"
    min = Number(min);
    var max = "999"
    max = Number(max);
    var currentMinPrice = "99"
    currentMinPrice = Number(currentMinPrice);
    var currentMaxPrice = "999"
    //alert('min: '+min+'--max: '+ max+ 'currentMin: '+currentMinPrice);
    currentMaxPrice = Number(currentMaxPrice);
    var params = "";
    params = $.trim(params);

    //slider (ajax)
    $("#slider-range").slider({
        range: true,
        min: min,
        max: max,
        values: [currentMinPrice, currentMaxPrice],
        slide: function (event, ui) {
            $("#amount").val(currencies + ui.values[0] + " - " + currencies + ui.values[1]);
            $('input[name="first_price"]').val(ui.values[0]);
            $('input[name="last_price"]').val(ui.values[1]);
        },
        stop: function (event, ui) {
            var first = ui.values[0];
            var last = ui.values[1];
            var baseUrl = 'http://demo4plazathemes.com/19/ma_flaton/index.php/laptop.html' + '?rate=' + rate + '&first=' + first + '&last=' + last + params;
            ajaxFilter(baseUrl);

        }
    });

    $("#amount").val(currencies + $("#slider-range").slider("values", 0) + " - " + currencies + $("#slider-range").slider("values", 1));
    $('input[name="first_price"]').val($("#slider-range").slider("values", 0));
    $('input[name="last_price"]').val($("#slider-range").slider("values", 1));

    //search price from input box
    $('#search_price').each(function () {
        $(this).live('click', function () {
            var first_price = $('input[name="first_price"]').val();
            var last_price = $('input[name="last_price"]').val();
            var rate = "1";
            var urlFilter = 'http://demo4plazathemes.com/19/ma_flaton/index.php/laptop.html' + '?rate=' + rate + '&first=' + first_price + '&' + 'last=' + last_price + params;
            ajaxFilter(urlFilter);
            return false;
        })
    });

    $('#slider-range a:first').addClass('first_item');
    $('#slider-range a:last').addClass('last_item');
});