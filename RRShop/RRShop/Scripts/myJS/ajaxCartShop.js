﻿$(document).ready(function () {
    $(".btn-remove2").on("click", function () {
        var proId = $(this).attr("data-proId");
        var price = unformatNumber($(this).parent().prevAll(".unitPrice").children().children().text());
        var tr = $(this).parent().parent();

        $.post(urlRemove, { "ProductId": proId }, function (data) {
            tr.remove();
            var cart = $("#mini_cart_block");
            var quantity = cart.find(".count-item");
            quantity.text(data.CartTotal);

            var subTotal = cart.find(".top-subtotal > span.price > span");
            var total = formatNumber(data.Total);
            subTotal.text(total);
            quantity.stop(true, true).removeAttr("style").effect("shake", { times: 2, distance: 5, direction: "up" }, "slow");

            $("#shopping-cart-totals-table #total > .a-right > strong > .price > span").text(total);
            $("#shopping-cart-totals-table #subTotal > .a-right > .price > span").text(total);
        });
    });

    $("#empty_cart_button").on("click", function () {
        var div = $(this).parents(".col-main.cart");

        $.post(urlRemoveAll, function (data) {
            div.children().remove();
            var cart = $("#mini_cart_block");
            var quantity = cart.find(".count-item");
            quantity.text(data.CartTotal);

            var subTotal = cart.find(".top-subtotal > span.price > span");
            var total = formatNumber(data.Total);
            subTotal.text(total);
            quantity.stop(true, true).removeAttr("style").effect("shake", { times: 2, distance: 5, direction: "up" }, "slow");
        });
    });

    $(".unitQuantity > .qty-spinner > input.qty").on("blur", function () {
        var $this = $(this);
        var quantity = $this.val() > $this.attr("max") ? parseInt($this.attr("max")) : $this.val();

        var proId = $this.parent().attr("data-proId");
        var price = unformatNumber($this.parents(".unitQuantity").prev().children().children().text());
        var amount = $this.parents(".unitQuantity").next().children().children();

        $.post(urlChange, { ProductId: proId, Quantity: quantity, Price: price }, function (data) {
            if (quantity == 0) {
                $this.parents("tr").remove();
            } else {
                amount.text(formatNumber(quantity * parseInt(price)));
            }

            var cart = $("#mini_cart_block");
            var cartQuantity = cart.find(".count-item");
            cartQuantity.text(data.CartTotal);

            var subTotal = cart.find(".top-subtotal > span.price > span");
            var total = formatNumber(data.Total);
            subTotal.text(total);
            cartQuantity.stop(true, true).removeAttr("style").effect("shake", { times: 2, distance: 5, direction: "up" }, "slow");

            $("#shopping-cart-totals-table #total > .a-right > strong > .price > span").text(total);
            $("#shopping-cart-totals-table #subTotal > .a-right > .price > span").text(total);
        });
    });

    /*ajax button decrease*/
    $(".qty-spinner > .btn-decrease").on("click", function () {
        var inputQty = $(this).next();
        var val = parseInt(inputQty.val());

        if (val > 1) {
            var proId = $(this).parent().attr("data-proId");
            var parent = $(this).parent().parent();
            var amount = parent.next().children().children(".price");
            var price = unformatNumber(parent.prev().children().children(".price").text());

            $.post(urlAdd, { ProductId: proId, Quantity: -1, Price: price }, function (data) {
                inputQty.val(val - 1);
                amount.text(formatNumber(unformatNumber(amount.text()) - price));

                var cart = $("#mini_cart_block");
                var quantity = cart.find(".count-item");
                quantity.text(data.CartTotal);

                var subTotal = cart.find(".top-subtotal > span.price > span");
                var total = formatNumber(data.Total);
                subTotal.text(total);
                quantity.stop(true, true).removeAttr("style").effect("shake", { times: 2, distance: 5, direction: "up" }, "slow");

                $("#shopping-cart-totals-table #total > .a-right > strong > .price > span").text(total);
                $("#shopping-cart-totals-table #subTotal > .a-right > .price > span").text(total);
            });
        }
    });

    /*ajax button increase*/
    $(".qty-spinner > .btn-increase").on("click", function () {
        var inputQty = $(this).prev();
        var val = parseInt(inputQty.val());

        if (val < parseInt(inputQty.attr("max"))) {
            var proId = $(this).parent().attr("data-proId");
            var parent = $(this).parent().parent();
            var amount = parent.next().children().children(".price");
            var price = unformatNumber(parent.prev().children().children(".price").text());

            $.post(urlAdd, { ProductId: proId, Quantity: 1, Price: price }, function (data) {
                inputQty.val(val + 1);
                amount.text(formatNumber(parseInt(unformatNumber(amount.text())) + parseInt(price)));

                var cart = $("#mini_cart_block");
                var quantity = cart.find(".count-item");
                quantity.text(data.CartTotal);

                var subTotal = cart.find(".top-subtotal > span.price > span");
                var total = formatNumber(data.Total);
                subTotal.text(total);
                quantity.stop(true, true).removeAttr("style").effect("shake", { times: 2, distance: 5, direction: "up" }, "slow");

                $("#shopping-cart-totals-table #total > .a-right > strong > .price > span").text(total);
                $("#shopping-cart-totals-table #subTotal > .a-right > .price > span").text(total);
            });
        }
    });
});