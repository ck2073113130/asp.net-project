﻿$('.ma-thumbnail-container .bxslider').bxSlider({
    slideWidth: 84,
    slideMargin: 12,
    minSlides: 4,
    maxSlides: 4,
    pager: false,
    speed: 500,
    pause: 3000
});