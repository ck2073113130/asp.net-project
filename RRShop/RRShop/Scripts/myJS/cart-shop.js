﻿$(".top-cart-contain").on("mouseenter", function () {
    $(this).find(".top-cart-content").stop(true, true).slideDown();
});

$(".top-cart-contain").on("mouseleave", function () {
    $(this).find(".top-cart-content").stop(true, true).slideUp();
});

$(".unitQuantity > .qty-spinner > input.qty").on("keypress", function (e) {
    return ((e.charCode >= 48 && e.charCode <= 57) || e.keyCode == 8
           || (e.keyCode >= 37 && e.keyCode <= 40) || e.keyCode == 46);
});