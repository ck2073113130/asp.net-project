﻿function formatNumber(data) {
    return data.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function unformatNumber(data) {
    return data.toString().replace(/,/g, "");
}