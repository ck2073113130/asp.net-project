﻿$(document).ready(function () {
    // Product Tabs in Home Index
    $(".producttabs-products-owl").owlCarousel({
        autoPlay: false,
        items: 4,
        itemsDesktop: [1199, 4],
        itemsDesktopSmall: [991, 3],
        itemsTablet: [700, 2],
        itemsMobile: [400, 1],
        slideSpeed: 3000,
        paginationSpeed: 3000,
        rewindSpeed: 3000,
        navigation: true,
        stopOnHover: true,
        pagination: true,
        scrollPerPage: true,
    });

    $('#producttabs li').click(function () {
        var pr_tab = $(this).find("h3").attr('class');
        $("#producttabs li").removeClass("active");
        $(this).addClass("active");
        $("#producttab_" + pr_tab).slideDown(400).siblings().slideUp(400);
    });
    // End

    // Hot Saler in Home Partial
    $(".ma-timer-container .owl").owlCarousel({
        autoPlay: false,
        items: 1,
        itemsDesktop: [1199, 1],
        itemsDesktopSmall: [991, 1],
        itemsTablet: [700, 1],
        itemsMobile: [400, 1],
        slideSpeed: 3000,
        paginationSpeed: 3000,
        rewindSpeed: 3000,
        navigation: false,
        stopOnHover: true,
        pagination: true,
        scrollPerPage: true,
        afterMove: function () {
            x = $(".ma-timer-container .owl-pagination .owl-page").index($(".ma-timer-container .owl-pagination .active"));
            var thumb = ".thumb" + x;
            $(".ma-timer-container .thumb li").removeClass('active');
            $(thumb).addClass('active');
        }
    });

    var owltimerslider = $(".ma-timer-container .owl").data('owlCarousel');

    $(".ma-timer-container .thumb li").on("click", function () {
        var x = $(this).attr("data-owl");
        owltimerslider.goTo(x)
    });
    // End

    // Feature Category in Home Partial
    $("#catthumb").owlCarousel({
        autoPlay: false,
        items: $(this).attr("catNum"),
        itemsDesktop: [1199, 5],
        itemsDesktopSmall: [991, 4],
        itemsTablet: [700, 3],
        itemsMobile: [479, 2],
        slideSpeed: 3000,
        paginationSpeed: 3000,
        rewindSpeed: 3000,
        navigation: true,
        stopOnHover: true,
        pagination: true,
        scrollPerPage: true,
    });
    // End

    //  Most View Tab in Home Partial
    $(".ma-mostviewedproductslider-container .owl").owlCarousel({
        autoPlay: false,
        items: 4,
        itemsDesktop: [1199, 3],
        itemsDesktopSmall: [991, 2],
        itemsTablet: [700, 2],
        itemsMobile: [400, 1],
        slideSpeed: 3000,
        paginationSpeed: 3000,
        rewindSpeed: 3000,
        navigation: true,
        stopOnHover: true,
        pagination: true,
        scrollPerPage: true,
    });
    // End

    //
    //$(".testimonial-sidebar .owl").owlCarousel({
    //    autoPlay: false,
    //    items: 1,
    //    itemsDesktop: [1199, 1],
    //    itemsDesktopSmall: [991, 1],
    //    itemsTablet: [700, 1],
    //    itemsMobile: [400, 1],
    //    slideSpeed: 3000,
    //    paginationSpeed: 3000,
    //    rewindSpeed: 3000,
    //    navigation: false,
    //    stopOnHover: true,
    //    pagination: true,
    //    scrollPerPage: true,
    //    afterMove: function () {
    //        x = $(".testimonial-sidebar .owl-pagination .owl-page").index($(".testimonial-sidebar .owl-pagination .active"));
    //        var testithumb = ".testithumb" + x;
    //        $(".testimonial-sidebar .thumb li").removeClass('active');
    //        $(testithumb).addClass('active');
    //    }
    //});

    //var owlslider = $(".testimonial-sidebar .owl").data('owlCarousel');

    //function testislider(x) {
    //    owlslider.goTo(x)
    //}
    // End

    //
    $(".menu-recent .owl").owlCarousel({
        autoPlay: false,
        items: 3,
        itemsDesktop: [1199, 3],
        itemsDesktopSmall: [991, 2],
        itemsTablet: [700, 2],
        itemsMobile: [400, 1],
        slideSpeed: 1000,
        paginationSpeed: 1000,
        rewindSpeed: 1000,
        navigation: true,
        stopOnHover: true,
        pagination: false,
        scrollPerPage: true,
    });
    // End

    //
    $(".ma-brand-slider-contain .owl").owlCarousel({
        autoPlay: true,
        items: 5,
        itemsDesktop: [1199, 5],
        itemsDesktopSmall: [991, 4],
        itemsTablet: [700, 4],
        itemsMobile: [400, 2],
        slideSpeed: 4000,
        paginationSpeed: 4000,
        rewindSpeed: 4000,
        navigation: true,
        stopOnHover: true,
        pagination: false,
        scrollPerPage: true,
    });
    // End

    // Related Area in Detail
    $(".ma-relatedslider-container .owl").owlCarousel({
        autoPlay: false,
        items: 1,
        itemsDesktop: [1199, 1],
        itemsDesktopSmall: [991, 1],
        itemsTablet: [700, 1],
        itemsMobile: [400, 1],
        slideSpeed: 3000,
        paginationSpeed: 3000,
        rewindSpeed: 3000,
        navigation: false,
        stopOnHover: true,
        pagination: true,
        scrollPerPage: true,
    });
    // End

    // 
    $(".ma-upsellslider-container .owl").owlCarousel({
        autoPlay: false,
        items: 4,
        itemsDesktop: [1199, 3],
        itemsDesktopSmall: [991, 2],
        itemsTablet: [767, 2],
        itemsMobile: [400, 1],
        slideSpeed: 3000,
        paginationSpeed: 3000,
        rewindSpeed: 3000,
        navigation: true,
        stopOnHover: true,
        pagination: true,
        scrollPerPage: true,
    });
    // End

});