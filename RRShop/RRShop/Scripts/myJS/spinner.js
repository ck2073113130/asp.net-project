﻿$(document).ready(function () {
    /*button decrease*/
    $(".qty-spinner > .btn-decrease").on("mouseenter", function () {
        $(this).stop(true, true).animate({
            "margin-right": "-1px"
        }).css({
            "text-align": "center",
            "padding-left": "7px"
        });
    });

    $(".qty-spinner > .btn-decrease").on("mouseleave", function () {
        $(this).stop(true, true).animate({
            "margin-right": "-16px"
        }).css({
            "text-align": "left",
            "padding-left": "4px"
        });
    });

    /*button increase*/
    $(".qty-spinner > .btn-increase").on("mouseenter", function () {
        $(this).stop(true, true).animate({
            "margin-left": "-1px"
        }).css({
            "text-align": "center",
            "padding-right": "7px"
        });
    });

    $(".qty-spinner > .btn-increase").on("mouseleave", function () {
        $(this).stop(true, true).animate({
            "margin-left": "-16px"
        }).css({
            "text-align": "right",
            "padding-right": "4px"
        });
    });
});