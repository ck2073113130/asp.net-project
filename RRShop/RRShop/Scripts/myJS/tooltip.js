﻿var count = 0;

$("div.item-inner > .images-container").each(function () {
    count++;

    $(this).children(".actions").children(".add-to-links")
           .append('<li class="qv-button-container"><a ' + 'class="qv-e-button btn-quickview-' + count + '"><em ' + 'class="tooltip">' + 'Xem nhanh</em>' + '</a></li>');
});

$(".tooltip").each(function () {
    $(this).css({
        "margin-left": -$(this).width() / 2
    });
});