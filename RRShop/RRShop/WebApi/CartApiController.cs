﻿using Connection;
using RRShop.ApiModels;
using RRShop.Business;
using RRShop.Models;
using System.Web.Http;
using System.Web.Http.Results;

namespace RRShop.WebApi
{
    [RoutePrefix("CartApi")]
    public class CartApiController : ApiController
    {
        [Route("Post")]
        [HttpPost]
        public JsonResult<object> Post([FromBody]CartItemApiModels item)
        {
            var result = CartShopViewModelsBUS.AddOrUpdateItem(item);

            return Json<object>(new { CartTotal = result.CartTotal, Total = result.Total });
        }

        [Route("Remove")]
        [HttpPost]
        public JsonResult<object> Remove([FromBody]CartItemApiModels item)
        {
            var result = CartShopViewModelsBUS.RemoveItem(item.ProductID);

            return Json<object>(new { CartTotal = result.CartTotal, Total = result.Total });
        }

        [Route("RemoveAll")]
        [HttpPost]
        public JsonResult<object> RemoveAll()
        {
            var result = CartShopViewModelsBUS.RemoveAllItems();

            return Json<object>(new { CartTotal = result.CartTotal, Total = result.Total });
        }

        [Route("Update")]
        [HttpPost]
        public JsonResult<object> Update([FromBody]CartItemApiModels item)
        {
            var result = CartShopViewModelsBUS.UpdateChange(item);

            return Json<object>(new { CartTotal = result.CartTotal, Total = result.Total });
        }
    }
}
