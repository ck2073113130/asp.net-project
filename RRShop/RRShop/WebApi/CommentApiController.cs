﻿using Business.Business;
using Connection;
using RRShop.ApiModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace RRShop.WebApi
{
    [RoutePrefix("CommentApi")]
    public class CommentApiController : ApiController
    {
        [Route("Post")]
        [HttpPost]
        public object AddComment([FromBody]Comment comment)
        {
            comment.PostedDate = DateTime.Now;
            comment.UserID = HttpContext.Current.Request.Cookies["UserId"] == null ? null : HttpContext.Current.Request.Cookies["UserId"].Value;

            CommentBUS.InsertComment(comment);
            return CommentBUS.GetCommentsByProductId(1, comment.ProductID);
        }

        [Route("ChangePage")]
        [HttpPost]
        public object ChangePage([FromBody]ChangePageApiModels model)
        {
            return CommentBUS.GetCommentsByProductId(model.Page, model.ProductId);
        }
    }
}
