﻿using Business.Business;
using RRShop.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace RRShop.WebApi
{
    [RoutePrefix("OrderApi")]
    public class OrderApiController : ApiController
    {
        [Route("Post")]
        [HttpPost]
        public string Post([FromBody] VerifyAddressViewModels address)
        {
            var cart = CartShopViewModels.GetCurrentCart();
            cart.Total -= (long)cart.CartItems.Where(c => c.RealQuantity == 0).Sum(c => (c.Quantity - c.RealQuantity) * c.Price);

            var result = InvoiceBUS.Insert(HttpContext.Current.Request.Cookies["UserId"].Value, address.Address, cart.Total, cart.CartItems.Where(c => c.RealQuantity != 0).ToList());

            return result ? "Chúc mừng bạn đã đặt hàng thành công." : "Bạn không đặt được sản phẩm nào cả";
        }
    }
}
